# Py Temp Log

This package is intended to be used with the I2C temperature and humidity sensors from IHTA.

It aims to provide a simple data logger interface to the sensors.

## CLI Tools

After installation, the [`TemperatureLogger`](py_temp_log/scripts/TemperatureLogger.py) app can be used to interface with any sensor controllers.
It also supports communication with multiple controllers.

## Pitfalls

The USB to serial adapter inside the controller boxes expects **no** serial control flow.
Otherwise the sensor results will not be transmitted back.
This also means, **no RTS**  and **no DTR**.

For pySerial this can be achieved like this

```python
ser = serial.serial_for_url('COM4', 9600, do_not_open=True)
ser.dtr = False
ser.rts = False
ser.open()
```

For testing, [hterm](https://www.der-hammer.info/pages/terminal.html) and [YAT](https://sourceforge.net/projects/y-a-terminal/) work out of the box.
