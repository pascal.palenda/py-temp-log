from typing import NamedTuple
from datetime import datetime


class Measurement(NamedTuple):
    temperature_1: float
    temperature_2: float
    temperature_3: float
    temperature_4: float
    temperature_5: float
    temperature_6: float
    temperature_7: float
    temperature_8: float
    humidity_6: float
    humidity_7: float
    humidity_8: float


class MeasurementPoint(NamedTuple):
    time: datetime
    data: dict[str, Measurement] = {}


def flatten(measurement_point: MeasurementPoint):
    flat_measurement_point = {
        f"{port}.{key}": value
        for port, measurement in measurement_point.data.items()
        for key, value in measurement._asdict().items()
    }
    flat_measurement_point["time"] = measurement_point.time.isoformat()
    return flat_measurement_point
