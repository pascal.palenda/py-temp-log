import py_temp_log as tl
import argparse
from pathlib import Path

def main():
    parser = argparse.ArgumentParser("TemperatureLogger",description="Log the temperature and humidity using the IHTA I2C measurement chains.")

    parser.add_argument("--output","-o", help="Output log file, no files will be overwritten",type=Path,default="TempLog.csv")
    parser.add_argument("ports", nargs="+", type=str, help="The serial ports to use")

    arguments = parser.parse_args()

    logger = tl.TempLog(arguments.output, ports=arguments.ports)

    logger.run()


if __name__ == "__main__":
    main()
