import serial
import csv
import sys
from pathlib import Path
import re
from .measurement_point import Measurement, MeasurementPoint, flatten
from datetime import datetime
from .util import create_valid_file_name
import sched
import time


class TempLog:
    def __init__(
        self, log_path: Path, ports: list[str], prevent_overwrite: bool = True
    ) -> None:
        if isinstance(log_path, str):
            log_path = Path(log_path)

        if log_path.exists() and prevent_overwrite:
            log_path = create_valid_file_name(log_path)

        self.log_path: Path = log_path
        self.csv_writer: csv.DictWriter = None
        self.file_handle = None
        self.scheduler: sched.scheduler = None

        self.connections: list[serial.Serial] = []
        for port in ports:
            try:
                new_serial = serial.serial_for_url(port, 9600, do_not_open=True)
                new_serial.dtr = False
                new_serial.rts = False
                new_serial.timeout = 1
                new_serial.open()
                self.connections.append(new_serial)
            except Exception as e:
                pass
                sys.stderr.write(
                    "--- ERROR opening new port {} with: {} ---\n".format(port, e)
                )
                new_serial.close()

    def measure(self) -> MeasurementPoint:
        measurement = MeasurementPoint(time=datetime.now())

        for connection in self.connections:
            connection.write(b"m")  # any character will trigger the measurement.

            raw_data = connection.readline()
            decoded_data = raw_data.decode()

            conditioned_data = re.sub(r"\s+", "", decoded_data).rstrip(";")
            separated_data = re.split(",|;", conditioned_data)

            data = [float(x) for x in separated_data]

            # The order in which the sensor data is send back from the Arduino is always the same.
            # T1;T2;T3;T4;T5;T6,H1;T7,H7;T8,H8
            measure = Measurement(
                temperature_1=data[0],
                temperature_2=data[1],
                temperature_3=data[2],
                temperature_4=data[3],
                temperature_5=data[4],
                temperature_6=data[5],
                temperature_7=data[7],
                temperature_8=data[9],
                humidity_6=data[6],
                humidity_7=data[8],
                humidity_8=data[10],
            )

            measurement.data[connection.port] = measure

        return measurement

    def log_measurement(self, measurement_point: MeasurementPoint) -> None:
        flat_measurement_point = flatten(measurement_point)

        dict_keys = list(flat_measurement_point.keys())
        dict_keys.insert(0, dict_keys.pop(dict_keys.index("time")))

        if self.csv_writer is None:
            self.file_handle = open(self.log_path, "a", newline="", buffering=1)
            self.csv_writer = csv.DictWriter(self.file_handle, dict_keys)
            self.csv_writer.writeheader()

        self.csv_writer.writerow(flat_measurement_point)

        self.file_handle.flush()

    def run(self, delay_seconds: float = 15):
        if self.scheduler is None:
            self.scheduler = sched.scheduler(time.time, time.sleep)

        def callback(
            scheduler: sched.scheduler,
            delay_seconds: float,
            logger: TempLog,
            start: datetime,
        ):
            scheduler.enter(
                delay_seconds, 1, callback, (scheduler, delay_seconds, logger, start)
            )
            logger.log_measurement(logger.measure())
            callback.counter += 1
            runtime = datetime.now() - start
            runtime = str(runtime).split(".")[0]
            print(
                f"Measurement Points: {callback.counter} - running for {runtime}",
                end="\r",
            )
            sys.stdout.flush()

        callback.counter = 0

        start = datetime.now()

        self.scheduler.enter(
            0, 1, callback, (self.scheduler, delay_seconds, self, start)
        )

        try:
            self.scheduler.run()
        except KeyboardInterrupt:
            pass

        print("")
