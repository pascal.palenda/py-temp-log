from pathlib import Path

def create_valid_file_name(file: Path, testMode = False):

    if testMode:
        return file

    if isinstance(file, str):
        file = Path(file)

    if not file.exists():
        return file

    i = 1

    new_file = file.with_name(file.stem + ".{:03}".format(i) + file.suffix)

    while new_file.exists():
        i += 1
        new_file = file.with_name(file.stem + ".{:03}".format(i) + file.suffix)

    return new_file